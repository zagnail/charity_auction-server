class BidType < ApplicationRecord
  has_many :donations

  validates :name, uniqueness: true, presence: true
end
