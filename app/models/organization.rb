class Organization < ApplicationRecord

  has_many :auctions

  has_many :memberships

  validates :name, presence: true
end
