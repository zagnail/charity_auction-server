class DonationCategory < ApplicationRecord

  validates :name, uniqueness: true, presence: true
end
