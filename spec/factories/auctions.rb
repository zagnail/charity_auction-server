FactoryGirl.define do
  factory :auction do
    association :organization, strategy: :build
    starts_at "2017-04-05 18:23:18"
    ends_at "2017-04-05 19:23:18"
    time_zone_id "America/New_York"
    physical_address "123 main St\nAnytown, NY 21201 USA"
    name "2016 Charity Auction"
    donation_window_ends_at "2017-04-04 18:23:18"
  end
end
