class AddOrganizationToActions < ActiveRecord::Migration[5.0]
  def change
    add_reference :auctions, :organization, index: true, foreign_key: true, null: false
  end
end
