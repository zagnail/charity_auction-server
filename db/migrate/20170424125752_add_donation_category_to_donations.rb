class AddDonationCategoryToDonations < ActiveRecord::Migration[5.0]
  def change
    add_reference :donations, :donation_category, foreign_key: true
  end
end
