class CreateDonationCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :donation_categories do |t|
      t.string :name

      t.timestamps null: false
    end
    add_index :donation_categories, :name, unique: true
  end
end
