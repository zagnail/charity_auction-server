class AddUniqueIndexToNameForBidTypes < ActiveRecord::Migration[5.0]
  def change
    add_index :bid_types, :name, unique: true
  end
end
