class CreateBidTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :bid_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
